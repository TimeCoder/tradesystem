﻿using System.Runtime.Serialization;

namespace Common
{
    /// <summary>
    /// Класс для сериализации WCF-исключения
    /// </summary>
    [DataContract]  
    public class InvalidDataFault   
    {   
        [DataMember]   
        public string Message { get; set; }

        public InvalidDataFault ()  
        {   
        }

        public InvalidDataFault(string error)  
        {   
            Message = error;   
        }   
    }
}