﻿using System.Data.Entity;
using System.Linq;
using DAL.Objects;

namespace DAL.Repositories
{
    /// <summary>
    /// Репозиторий, позволяющий работать с объектами из слоя сервисов
    /// </summary>
    public class Repository 
    {
        private readonly Context _dbContext;

        public IQueryable<Price> Prices
        {
            get { return _dbContext.Prices; } 
        }

        public IQueryable<Product> Products
        {
            get { return _dbContext.Products; }
        }

        public IQueryable<PriceItem> PriceItems
        {
            get { return _dbContext.PriceItems; }
        }

        /// <summary>
        /// ctor
        /// </summary>
        public Repository()
        {
            Database.SetInitializer(new ContextInitializer());
            _dbContext = new Context();
            _dbContext.Configuration.LazyLoadingEnabled = false;
        }

        /// <summary>
        /// Удаление позиции прайса
        /// </summary>
        /// <param name="priceItemId">Id записи</param>
        public void DeletePriceItem(int priceItemId)
        {
            _dbContext.PriceItems.Remove(_dbContext.PriceItems.FirstOrDefault(item => item.Id == priceItemId));
        }
        
        /// <summary>
        /// Сохранение изменений в БД
        /// </summary>
        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}