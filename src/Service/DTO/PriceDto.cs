﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Service.DTO
{
    /// <summary>
    /// DTO прайса
    /// </summary>
    [DataContract]
    public class PriceDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}