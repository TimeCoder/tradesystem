﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Catel.Data;
using Service.DTO;
using Shell.TradeServiceReference;

namespace Shell.Models
{
    /// <summary>
    /// Модель основных бизнес-процессов
    /// </summary>
	public class TradeModel : ModelBase
	{
        /// <summary>
        /// Клиент WCF-сервиса
        /// </summary>
		private readonly TradeServiceClient _tradeService;

        /// <summary>
        /// Список прайсов
        /// </summary>
		public ObservableCollection<PriceDto> Prices
		{
            get { return GetValue<ObservableCollection<PriceDto>>(PricesProperty); }
			set { SetValue(PricesProperty, value); }
		}
        public static readonly PropertyData PricesProperty = RegisterProperty("Prices", typeof(ObservableCollection<PriceDto>));

        /// <summary>
        /// Текущий выбранный прайс
        /// </summary>
        public PriceDto CurrentPrice
        {
            get { return GetValue<PriceDto>(CurrentPriceProperty); }
            set
            {
                SetValue(CurrentPriceProperty, value);

                if (value == null)
                    PriceItems = null;
                else
                {
                    Task.Run(async () =>
                    {
                        await LoadPrice();
                    });
                }
            }
        }
	    public static readonly PropertyData CurrentPriceProperty = RegisterProperty("CurrentPrice", typeof(PriceDto));

        /// <summary>
        /// Позиции выбранного прайса
        /// </summary>
        public ObservableCollection<PriceItemDto> PriceItems
        {
            get { return GetValue<ObservableCollection<PriceItemDto>>(PriceItemsProperty); }
            set { SetValue(PriceItemsProperty, value); }
        }
        public static readonly PropertyData PriceItemsProperty = RegisterProperty("PriceItems", typeof(ObservableCollection<PriceItemDto>));

        /// <summary>
        /// ctor
        /// </summary>
		public TradeModel()
		{
		    _tradeService = new TradeServiceClient();

            Task.Run(async () =>
            {
                Prices = new ObservableCollection<PriceDto>(await _tradeService.GetPricesAsync());
            });
		}

        /// <summary>
        /// Перезагрузка прайса
        /// </summary>
        public async Task LoadPrice()
        {
            PriceItems = new ObservableCollection<PriceItemDto>(await _tradeService.GetPriceItemsAsync(CurrentPrice.Id));
        }

        /// <summary>
        /// Сохранение изменений
        /// </summary>
	    public async Task SavePrice()
	    {
	        await _tradeService.SavePriceItemsAsync(PriceItems.ToArray());
	    }

        /// <summary>
        /// Удаление позиции
        /// </summary>
        public async Task DeletePriceItem(int priceItemId)
        {
            await _tradeService.DeletePriceItemAsync(priceItemId);
        }


	}
}
