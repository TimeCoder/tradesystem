﻿using System.Collections.ObjectModel;
using System.ServiceModel;
using Catel.Data;
using Catel.MVVM.Services;
using Common;
using Service.DTO;
using Shell.Models;

namespace Shell.ViewModels
{
    using Catel.MVVM;

    /// <summary>
    /// ViewModel основго окна
    /// </summary>
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IMessageService _messageService;
     
        /// <summary>
        /// Бизнес-модель
        /// </summary>
        [Model]
		public TradeModel Model
		{
			get { return GetValue<TradeModel>(ModelProperty); }
			set { SetValue(ModelProperty, value); }
		}
		public static readonly PropertyData ModelProperty = RegisterProperty("Model", typeof(TradeModel));


        /// <summary>
        /// Прайсы
        /// </summary>
        [ViewModelToModel("Model")]
        public ObservableCollection<PriceDto> Prices
        {
            get { return GetValue<ObservableCollection<PriceDto>>(PricesProperty); }
            set { SetValue(PricesProperty, value); }
        }
        public static readonly PropertyData PricesProperty = RegisterProperty("Prices", typeof(ObservableCollection<PriceDto>));


        /// <summary>
        /// Текущий прайс
        /// </summary>
        [ViewModelToModel("Model")]
        public PriceDto CurrentPrice
        {
            get { return GetValue<PriceDto>(CurrentPriceProperty); }
            set { SetValue(CurrentPriceProperty, value); }
        }
        public static readonly PropertyData CurrentPriceProperty = RegisterProperty("CurrentPrice", typeof(PriceDto));


        /// <summary>
        /// Позиции прайса 
        /// </summary>
        [ViewModelToModel("Model")]
        public ObservableCollection<PriceItemDto> PriceItems
        {
            get { return GetValue<ObservableCollection<PriceItemDto>>(PriceItemsProperty); }
            set { SetValue(PriceItemsProperty, value); }
        }
        public static readonly PropertyData PriceItemsProperty = RegisterProperty("PriceItems", typeof(ObservableCollection<PriceItemDto>));


        /// <summary>
        /// Выбранная позиция прайса
        /// </summary>
        public PriceItemDto CurrentPriceItem
        {
            get { return GetValue<PriceItemDto>(CurrentPriceItemProperty); }
            set { SetValue(CurrentPriceItemProperty, value); }
        }
        public static readonly PropertyData CurrentPriceItemProperty = RegisterProperty("CurrentPriceItem", typeof(PriceItemDto));


        /// <summary>
        /// ctor
        /// </summary>
        public MainWindowViewModel(IMessageService messageService)
        {
            _messageService = messageService;
            Model = new TradeModel();
        }

        /// <summary>
        /// Обновить прайс
        /// </summary>
        private Command _reloadCommand;
        public Command ReloadCommand
        {
            get
            {
                return _reloadCommand ?? (_reloadCommand = new Command(async () => await Model.LoadPrice(), () => CurrentPrice != null));
            }
        }

        /// <summary>
        /// Удалить позицию в прайсе
        /// </summary>
        private Command _deleteCommand;
        public Command DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new Command(async () =>
                {
                    await Model.DeletePriceItem(CurrentPriceItem.Id);
                    await Model.LoadPrice();
                },
                () => CurrentPriceItem != null));
            }
        }

        /// <summary>
        /// Сохранить изменения
        /// </summary>
        private Command _saveCommand;
        public Command SaveCommand
        {
            get
            {
                return _saveCommand ?? (_saveCommand = new Command(async () =>
                {
                    try
                    {
                        await Model.SavePrice();
                    }
                    catch (FaultException<InvalidDataFault> exception)
                    {
                        _messageService.ShowError("Ошибка при сохранении объекта");
                    }
                }, 
                () => CurrentPrice != null));
            }
        }


    }
}
