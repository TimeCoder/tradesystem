﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Objects;

namespace DAL.Repositories
{
    /// <summary>
    /// Контекст работы с объектами через EF
    /// </summary>
    class Context : DbContext
    {
        public DbSet<Price> Prices { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<PriceItem> PriceItems { get; set; }
    }
    
    
}
