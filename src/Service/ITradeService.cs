﻿using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using Common;
using Service.DTO;

namespace Service
{
    /// <summary>
    /// Интерфейс основного сервиса
    /// </summary>
    [ServiceContract]
    public interface ITradeService
    {
        /// <summary>
        /// Получить список прайсов
        /// </summary>
        [OperationContract]
        PriceDto[] GetPrices();

        /// <summary>
        /// Получить позиции прайса
        /// </summary>
        [OperationContract]
        PriceItemDto[] GetPriceItems(int priceId);

        /// <summary>
        /// Обновить в БД измененные позиции прайса
        /// </summary>
        [OperationContract]
        [FaultContract(typeof(InvalidDataFault))]
        void SavePriceItems(PriceItemDto[] priceItems);

        /// <summary>
        /// Удалить позицию прайса
        /// </summary>
        [OperationContract]
        void DeletePriceItem(int priceItemId);
    }


}
