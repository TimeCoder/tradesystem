﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Objects
{
    /// <summary>
    /// Объект БД: продукт
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Ключ
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        [MaxLength(10)]
        public string Code { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}