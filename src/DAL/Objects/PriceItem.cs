﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Objects
{
    /// <summary>
    /// Объект БД: позиция в прайсе
    /// </summary>
    public class PriceItem
    {
        /// <summary>
        /// Ключ
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Ссылка на прайс
        /// </summary>
        public Price Price { get; set; }

        /// <summary>
        /// Ссылка на продукт
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public double Cost { get; set; }

        /// <summary>
        /// Срок действия цены
        /// </summary>
        public DateTime ExperationDate { get; set; }
    }
}