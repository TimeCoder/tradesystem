﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using DAL.Objects;

namespace DAL.Repositories
{
    /// <summary>
    /// Класс заполнения БД демо-данными (когда меняется модель)
    /// </summary>
    internal class ContextInitializer : DropCreateDatabaseIfModelChanges<Context> 
    {
        protected override void Seed(Context context)
        {
		    #region DemoData

		    var prices = new List<Price>
		    {
			    new Price() {Code = "0001", Description = "Это прайс по умолчанию"},
			    new Price() {Code = "0002", Description = "Это некий другой прайс"},
		    };

		    var products = new List<Product>
		    {
			    new Product() {Code = "0001", Description = "Булка хлеба"},
			    new Product() {Code = "0002", Description = "Бумажный конверт"},
			    new Product() {Code = "0003", Description = "Канцелярский клей"},
			    new Product() {Code = "0004", Description = "Стеклянный стакан"},
			    new Product() {Code = "0005", Description = "Бумага для ксерокопий"},
			    new Product() {Code = "0006", Description = "Коврик для мыши"},
			    new Product() {Code = "0007", Description = "Клавиатура стандартная"},
			    new Product() {Code = "0008", Description = "Кресло офисное"},
			    new Product() {Code = "0009", Description = "Кондиционер напольный"},
			    new Product() {Code = "0010", Description = "Лицензия на ReSharper"},
		    };

		    var priceItems = new List<PriceItem>
		    {
			    new PriceItem {Price = prices[0], Product = products[0], Cost = 20.5, ExperationDate = new DateTime(2014, 8, 5)},
			    new PriceItem {Price = prices[0], Product = products[1], Cost = 2.3, ExperationDate = new DateTime(2014, 8, 5)},
			    new PriceItem {Price = prices[0], Product = products[2], Cost = 19.8, ExperationDate = new DateTime(2014, 8, 7)},
			    new PriceItem {Price = prices[0], Product = products[3], Cost = 20.5, ExperationDate = new DateTime(2014, 8, 10)},
			    new PriceItem {Price = prices[0], Product = products[4], Cost = 35.0, ExperationDate = new DateTime(2014, 8, 5)},
			    new PriceItem {Price = prices[0], Product = products[5], Cost = 272.0, ExperationDate = new DateTime(2014, 8, 8)},
			    new PriceItem {Price = prices[0], Product = products[6], Cost = 82.0, ExperationDate = new DateTime(2014, 8, 15)},
			    new PriceItem {Price = prices[0], Product = products[7], Cost = 122.99, ExperationDate = new DateTime(2014, 9, 12)},
			    new PriceItem {Price = prices[0], Product = products[8], Cost = 9999.99, ExperationDate = new DateTime(2014, 10, 11)},
			    new PriceItem {Price = prices[0], Product = products[9], Cost = 3020.0, ExperationDate = new DateTime(2014, 8, 17)},

			    new PriceItem {Price = prices[1], Product = products[0], Cost = 22.5, ExperationDate = new DateTime(2014, 9, 5)},
			    new PriceItem {Price = prices[1], Product = products[1], Cost = 2.9, ExperationDate = new DateTime(2014, 9, 5)},
			    new PriceItem {Price = prices[1], Product = products[2], Cost = 23.5, ExperationDate = new DateTime(2014, 9, 7)},
			    new PriceItem {Price = prices[1], Product = products[3], Cost = 22.7, ExperationDate = new DateTime(2014, 9, 10)},
			    new PriceItem {Price = prices[1], Product = products[4], Cost = 37.2, ExperationDate = new DateTime(2014, 9, 5)},
			    new PriceItem {Price = prices[1], Product = products[5], Cost = 281.0, ExperationDate = new DateTime(2014, 9, 8)},
			    new PriceItem {Price = prices[1], Product = products[6], Cost = 95.5, ExperationDate = new DateTime(2014, 9, 15)},
			    new PriceItem {Price = prices[1], Product = products[7], Cost = 142.99, ExperationDate = new DateTime(2014, 10, 12)},
			    new PriceItem {Price = prices[1], Product = products[9], Cost = 3270.0, ExperationDate = new DateTime(2014, 9, 17)},
		    };

		    #endregion

		    context.Prices.AddRange(prices);
		    context.SaveChanges();

		    context.Products.AddRange(products);
		    context.SaveChanges();

		    context.PriceItems.AddRange(priceItems);
		    context.SaveChanges();
	    }



    }
}