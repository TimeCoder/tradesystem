﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Objects
{
    /// <summary>
    /// Объект БД: прайс
    /// </summary>
    public class Price
    {
        /// <summary>
        /// Первичный ключ
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Код прайса
        /// </summary>
        [MaxLength(10)]
        public string Code { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}