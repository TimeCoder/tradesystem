﻿using System.Linq;
using System.ServiceModel;
using Common;
using DAL.Repositories;

using Service.DTO;

namespace Service
{
    /// <summary>
    /// Реализация основного сервиса
    /// </summary>
    public class TradeService : ITradeService
    {
        /// <summary>
        /// Репозиторий данных
        /// </summary>
        private readonly Repository _repository;

        /// <summary>
        /// ctor
        /// </summary>
        public TradeService()
        {
            _repository = new Repository();
        }


        public PriceDto[] GetPrices()
        {
	        return _repository.Prices
                .Select(price => new PriceDto
                {
                    Id = price.Id,
                    Code = price.Code,
                    Description = price.Description
                })
                .ToArray();
        }


        public PriceItemDto[] GetPriceItems(int priceId)
        {
            // Из позиции прайса и продукта мы делаем плоский DTO-объект,
            // который удобно сериализовать
            var res = _repository.PriceItems
                .Where(item => item.Price.Id == priceId)
                .Select(item => new PriceItemDto
                {
                    Id = item.Id,
                    ProductId = item.Product.Id,
                    ProductCode = item.Product.Code,
                    ProductDescription = item.Product.Description,
                    Cost = item.Cost,
                    ExperationDate = item.ExperationDate,
                })
                .ToArray();
            return res;
        }

        
        public void SavePriceItems(PriceItemDto[] priceItems)
        {
            // Проходим по всем позициям
            foreach (var priceItemDto in priceItems)
            {
                var priceItem = _repository.PriceItems.FirstOrDefault(item => item.Id == priceItemDto.Id);
                if (priceItem == null)
                    throw new FaultException<InvalidDataFault>(new InvalidDataFault());

                var product = _repository.Products.FirstOrDefault(item => item.Id == priceItemDto.ProductId);
                if (product == null)
                    throw new FaultException<InvalidDataFault>(new InvalidDataFault());
                
                // Обновляем позицию прайса
                priceItem.Cost = priceItemDto.Cost;
                priceItem.ExperationDate = priceItemDto.ExperationDate;

                // Обновляем продукт
                product.Code = priceItemDto.ProductCode;
                product.Description = priceItemDto.ProductDescription;
            }

            try
            {
                // Если EF валидация выдаст ошибки
                _repository.Save();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                // То мы попадем сюда
                var errorMessages =
                    e.EntityValidationErrors.Select(er => er.ValidationErrors.SelectMany(err => err.ErrorMessage));
                
                var message = string.Join("\n", errorMessages);
                throw new FaultException<InvalidDataFault>(new InvalidDataFault(message), message);
            }
        }


        public void DeletePriceItem(int priceItemId)
        {
            _repository.DeletePriceItem(priceItemId);
            _repository.Save();
            
        }
    }
}
