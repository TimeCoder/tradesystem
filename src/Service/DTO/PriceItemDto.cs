﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Service.DTO
{
    /// <summary>
    /// DTO элемента прайса
    /// </summary>
    [DataContract]
    public class PriceItemDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public string ProductCode { get; set; }

        [DataMember]
        public string ProductDescription { get; set; }

        [DataMember]
        public double Cost { get; set; }

        [DataMember]
        public DateTime ExperationDate { get; set; }
    }
}